#include<iostream>

using namespace std;

int main(){
	
	//Declaration of variable
	int myFirstVariable;
	float myFirstFloat;
	double myFirstDouble;
	char myFirstChar;
	
	//Initialize
	myFirstVariable = 1;
	myFirstFloat = 2.123456789123456789;
	myFirstDouble = 2.12;
	myFirstChar = 'c';
	
	//Display of the cariable
	cout<<myFirstVariable<<endl;
	cout<<myFirstFloat<<endl;
	cout<<myFirstDouble<<endl;
	cout<<myFirstChar;
	
	return 0;
}
