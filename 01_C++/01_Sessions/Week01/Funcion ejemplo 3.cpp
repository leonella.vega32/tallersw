#include<iostream>

using namespace std;
/*
Funcion efemplo 03
*/
int main(){
	//Delaration of example 03
	int numberImput;
	int resultSum;
	int resultRest;
	int resultMult;
	
	//initrialize
	numberImput = 0;
	resultSum = 0;
	resultRest = 0;
	resultMult = 0;
	
	//Display phrase 1
	cout<<"escriba un numero entero : ";
	cin>>numberImput;
	
	//Operation
	resultSum = numberImput+5;
	resultRest = numberImput-5;
	resultMult = numberImput*5;
	
	//Display phrase 2 with number
	cout<<"el resultado de la suma : "<<resultSum;
	cout<<"\n\rrel resultado de la resta es : "<<resultRest;
	cout<<"\n\rel resultado de la multiplicacion : "<<resultMult;
	
	return 0;
}

