#include<iostream>

using namespace std;
/*
1. Operacion que hace operaciones y puede rtornar valores
2. Reutilizable
*/

//Declaracion de funcion
void Saludar(void);
int SumarTresNumeros(int numero1, int numero2, int numero3);

int main(){
	
	Saludar();
	
	int res = 0;
	
	res = SumarTresNumeros(7,3,1);
	cout<<res;
	return 0;
}

// Definicion de funcion
void Saludar(void){
	cout<<"Hola mundo desde la funcion\n\r";
}

int SumarTresNumeros(int numero1, int numero2, int numero3){
	int resultado = 0;
	resultado= numero1 + numero2 + numero3;
	return resultado;
	
}

//int SumarTresNumeros(int numero1, int numero2, int numero3){
//	return numero1 + numero2 + numero3; 
//}


