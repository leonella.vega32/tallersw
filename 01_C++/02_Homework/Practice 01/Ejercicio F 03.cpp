/**
 * @file Ejercicio F 03.cpp
 * @author Leonella Vega (leonella.vega32@gmail.com)
 * @brief Excercise funciones 03:
 		 Queremos conocer los datos estad�sticos de una asignatura, 
		   por lo tanto, necesitamos un algoritmo que lea el n�mero de desaprobados, 
		   aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
           a. El tanto por ciento de alumnos que han superado la asignatura.
           b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.

 * @version 1.0
 * @date 18/12/21
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
	int studentsDesapproved = 0;          
	int studentsApproved = 0;           
	int studentsNotable = 0;             
	int studentsOutstanding = 0;     
	float porcentageDesapproved = 0.0;    
	float porcentageApproved = 0.0;        
	float porcentageNotable = 0.0;        
	float porcentageOutstanding = 0.0;   
	int totalStudents = 0;             
	int studentsPassed = 0;       
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int CalculateTotalStudents(int studentsDesapproved,int studentsApproved,int studentsNotable,int studentsOutstanding);
float CalculatePorcentageApproved(float studentsApproved,float totalStudents);
float CalculatePorcentageDesapproved(float studentsDesapproved,float totalStudents);
float CalculatePorcentageNotable(float studentsNotable,float totalStudents);
float CalculatePorcentageOutstanding(float studentsOutstanding,float totalStudents);
float CalculatePorcentagePassed(float studentsApproved,float studentsNotable,float studentsOutstanding);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Numeros de alumnos desaprobado : ";
	cin>>studentsDesapproved;
	cout<<"\n\rNumeros de alumnos aprobados : ";
	cin>>studentsApproved;
	cout<<"\n\rNumeros de alumnos Notables : ";
	cin>>studentsNotable;
	cout<<"\n\rNumeros de alumnos sobresalientes : ";
	cin>>studentsOutstanding;
}
//=====================================================================================================

void Calculate(){
	totalStudents = CalculateTotalStudents(studentsDesapproved,studentsApproved,studentsNotable,studentsOutstanding);
	porcentageApproved = CalculatePorcentageApproved(studentsApproved,totalStudents);
	porcentageDesapproved = CalculatePorcentageDesapproved(studentsDesapproved,totalStudents);
	porcentageNotable = CalculatePorcentageNotable(studentsNotable,totalStudents);
	porcentageOutstanding = CalculatePorcentageOutstanding(studentsOutstanding,totalStudents);
	studentsPassed = CalculatePorcentagePassed(studentsApproved,studentsNotable,studentsOutstanding); 
}
//=====================================================================================================

void ShowResults(){
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"\n\rCantidad de alumnos que superaron la asignatura es : "<<studentsPassed;
	cout<<"\n\rEl porcentaje de alumnos desaprobados es : "<<porcentageDesapproved<<"%";
	cout<<"\n\rEl porcentaje de alumnos aprobados es : "<<porcentageApproved<<"%";
	cout<<"\n\rEl porcentaje de alumnos notables es : "<<porcentageNotable<<"%";
	cout<<"\n\rEl porcentaje de alumnos sobresalientes es : "<<porcentageOutstanding<<"%";
}
//=====================================================================================================


int CalculateTotalStudents(int studentsDesapproved,int studentsApproved,int studentsNotable,int studentsOutstanding){
	return studentsDesapproved+studentsApproved+studentsNotable+studentsOutstanding;
}

//=====================================================================================================
float CalculatePorcentageApproved(float studentsApproved,float totalStudents){
	return ((float)studentsApproved)/((float)totalStudents)*100;
}

//=====================================================================================================
float CalculatePorcentageDesapproved(float studentsDesapproved,float totalStudents){
	return ((float)studentsDesapproved/(float)totalStudents)*100;
}

//=====================================================================================================
float CalculatePorcentageNotable(float studentsNotable,float totalStudents){
	return ((float)studentsNotable)/((float)totalStudents)*100;
}

//=====================================================================================================
float CalculatePorcentageOutstanding(float studentsOutstanding,float totalStudents){
	return ((float)studentsOutstanding)/((float)totalStudents)*100;
}

//=====================================================================================================
float CalculatePorcentagePassed(float studentsApproved,float studentsNotable,float studentsOutstanding){
	return (studentsApproved+studentsNotable+studentsOutstanding); 
}

