/**
 * @file Ejercicio 02.cpp
 * @author Leonella Vega (leonella.vega32@gmail.com)
 * @brief exercise 02
        Un maestro desea saber que porcentaje de hombres y que porcentaje
		de mujeres hay en un grupo de estudiantes.
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	//Declaration and initialization of variables
	int numberAlumnos = 0;           
	int numberWoman = 0;            
	int numberMen = 0;              
	float porcentageWoman = 0.0;      
	float porcentageMen = 0.0;        
	
	
	//Ask data throught the terminal
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Escriba la cantidad de alumnos : ";
	cin>>numberAlumnos;
	cout<<"\n\rEscriba la cantidad de mujeres : ";
	cin>>numberWoman;
	
	
	//Calculation
	numberMen = numberAlumnos-numberWoman;
	porcentageWoman = ((float)numberWoman)/((float)numberAlumnos)*100;
	porcentageMen = ((float)numberMen/(float)numberAlumnos)*100;
	
	//Show results in terminal
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"\n\rLa cantidad de varones es : "<<numberMen;
	cout<<"\n\rPorcentaje de mujeres : "<<porcentageWoman<<"%";
	cout<<"\n\rPorcentaje de varones : "<<porcentageMen<<"%";

	
	return 0;
}
