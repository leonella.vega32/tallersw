/**
 * @file Ejercicio 03.cpp
 * @author Leonella Vega (leonella.vega32@gmail.com)
 * @brief exercise 03
           Queremos conocer los datos estad�sticos de una asignatura, 
		   por lo tanto, necesitamos un algoritmo que lea el n�mero de desaprobados, 
		   aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
           a. El tanto por ciento de alumnos que han superado la asignatura.
           b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.

 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	//Declaration and initialization of variables
	int studentsDesapproved = 0;          
	int studentsApproved = 0;           
	int studentsNotable = 0;             
	int studentsOutstanding = 0;     
	float porcentageDesapproved = 0.0;    
	float porcentageApproved = 0.0;        
	float porcentageNotable = 0.0;        
	float porcentageOutstanding = 0.0;   
	int totalStudents = 0;             
	int studentsPassed = 0;           
	
	//Ask data throught the terminal
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Numeros de alumnos desaprobado : ";
	cin>>studentsDesapproved;
	cout<<"\n\rNumeros de alumnos aprobados : ";
	cin>>studentsApproved;
	cout<<"\n\rNumeros de alumnos Notables : ";
	cin>>studentsNotable;
	cout<<"\n\rNumeros de alumnos sobresalientes : ";
	cin>>studentsOutstanding;
	
	//Calculation
	totalStudents = studentsDesapproved+studentsApproved+studentsNotable+studentsOutstanding;
	porcentageApproved = ((float)studentsApproved)/((float)totalStudents)*100;
	porcentageDesapproved = ((float)studentsDesapproved/(float)totalStudents)*100;
	porcentageNotable = ((float)studentsNotable)/((float)totalStudents)*100;
	porcentageOutstanding = ((float)studentsOutstanding)/((float)totalStudents)*100;
	studentsPassed = studentsApproved+studentsNotable+studentsOutstanding;
	
	//Show results in terminal
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"\n\rCantidad de alumnos que superaron la asignatura es : "<<studentsPassed;
	cout<<"\n\rEl porcentaje de alumnos desaprobados es : "<<porcentageDesapproved<<"%";
	cout<<"\n\rEl porcentaje de alumnos aprobados es : "<<porcentageApproved<<"%";
	cout<<"\n\rEl porcentaje de alumnos notables es : "<<porcentageNotable<<"%";
	cout<<"\n\rEl porcentaje de alumnos sobresalientes es : "<<porcentageOutstanding<<"%";
	
	return 0;
}
