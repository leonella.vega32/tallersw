/**
 * @file Ejercicio 06.cpp
 * @author Leonella Vega (leonella.vega32@gmail.com)
 * @brief exercise 06
           Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos.

 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	//Declaration and initialization of variable
	int imputSeconds = 0;
	float imputHours = 0.0;
	float imputDays = 0.0;
	float imputMinuts = 0.0;
	
	//Ask data throught the terminal
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"ingresar los segundos : ";
	cin>>imputSeconds;
	
	//Calculation
	imputHours=(float)imputSeconds/3600;
	imputDays=(float)imputSeconds/86400;
	imputMinuts=(float)imputSeconds/60;
	
	//Show result in terminal
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"Su equivalencia en dias es : "<<imputDays;
	cout<<"\n\rsu equivalencia en horas es : "<<imputHours;
	cout<<"\n\rSu equivalencia en minutos es : "<<imputMinuts;
	
	return 0;
}
