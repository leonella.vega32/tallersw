/**
 * @file Ejercicio 05.cpp
 * @author Leonella Vega (leonella.veg32@gmail.com)
 * @brief Exercise 05:
        El costo de un autom�vil nuevo para un comprador es la suma total del costo del
		veh�culo, del porcentaje de la ganancia del vendedor y de los impuestos locales
		o estatales aplicables (sobre el precio de venta). Suponer una ganancia del
		vendedor del 12% en todas las unidades y un impuesto del 6% y dise�ar un
		algoritmo para leer el costo total del autom�vil e imprimir el costo para
		el consumidor.
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	//Declaration and initialization or variables
	float costoVehiculo = 0.0;      //Costo del vehiculo 
	float costoConsumidor = 0.0;    //Costo para el consumido, a�adiendole 12% de comicion y 6% de impuesto
	
	//Ask date throught the terminal
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Ingrese el costo del vehiculo : ";
	cin>>costoVehiculo;
	
	//calculation
	costoConsumidor=(120*106*costoVehiculo)/10000;
	
	//Show result in terminal
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"El costo para el consumidor es: "<<costoConsumidor;
	
	return 0;
}
