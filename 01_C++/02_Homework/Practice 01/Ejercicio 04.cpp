/**
 * @file Ejercicio 04.cpp
 * @author Leonella Vega (leonella.veg32@gmail.com)
 * @brief Exercise 04:
        Un departamento de climatolog�a ha realizado recientemente su conversi�n al 
		sistema m�trico. Dise�ar un algoritmo para realizar las siguientes conversiones:
        a. Leer la temperatura dada en la escala Celsius e imprimir en su equivalente
		 Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
        b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en mil�metros
		 (25.5 mm = 1pulgada).
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	//Declaratiom and initialization of variables
	float temperatureC = 0.0;      //Temperature in degrees celsius
	float temperatureF = 0.0;      //Temperature in degrees fahrenheites
	float waterPul = 0.0;          //Amount of water in inches
	float waterMm = 0.0;           //Amount of warwe in mm
	
	//Ask data throught the terminal
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Ingrese la temperatura en grado Celsius : ";
	cin>>temperatureC;
	cout<<"Ingrese la cantidad de agua en pulgadas : ";
	cin>>waterPul;
	
	//Calculation
	temperatureF=((9*temperatureC)/5)+32;
	waterMm=25.5*waterPul;
	
	//Show result in terminal
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"La temperatura en fahrenheites : "<<temperatureF;
	cout<<"\n\rLa cantidad de agua en mm es : "<<waterMm;
	
	return 0;
}
