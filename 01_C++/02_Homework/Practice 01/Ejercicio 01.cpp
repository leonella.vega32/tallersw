/**
 * @file Ejercicio 01.cpp
 * @author Leonella Vega (leonella.vega32@gmail.com)
 * @brief exercise 01
           Elabore un algoritmo que dados como datos  de entrada el radio y 
		   la altura de un cilindro calcular, el rea lateral y el volumen del cilindro.
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include<math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/

#define PI 3.1416     //constante pi

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	//Declaration and initialization of variables
	float imputRadio = 0.0;        
	float imputAltura = 0.0;         
	float resulArea = 0.0;         //area lateral del ciclindro calculada 
	float resulVolumen = 0.0;      //volumen del cilindro  calculada
	
	//Ask data throught the terminal
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Escribir el radio del cilindro: ";
	cin>>imputRadio;
	cout<<"\n\rEscribir la altura del cilindro: ";
	cin>>imputAltura;
	
	//Calculation
	resulArea = 2.0*PI*imputRadio*imputAltura;              //area latera = 2*pi*radio*altura
	resulVolumen = PI*pow(imputRadio,2.0)*imputAltura;      //volume = pi*radio^2*altura   
	
	//Show results in terminal
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"\n\rEl area lateral del cilindro es : "<<resulArea;
	cout<<"\n\rEl volumen del cilindro es : "<<resulVolumen;
	
	return 0;
}






