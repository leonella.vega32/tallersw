/**
 * @file Ejercicio 07.cpp
 * @author Leonella Vega (leonella.veg32@gmail.com)
 * @brief Exercise 07:
        Calcular la fuerza de atraccion entre dos masas, separadas por una distancia
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include<math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define G 6.673*pow(10,-8)    //Constante de gravitacion universal

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
    //Declaration and Initialization of variables
	double imputMasa1 = 0.0;         //entrada de masa 1 a traves del terminal
	double imputMasa2 = 0.0;         //entrada de masa 2 a traves del terminal
	double imputDistancia = 0.0;     //entrada de distancia entre las masas a traves del terminal
	double imputFuerza = 0.0;        //Fuerza de atraccion entre las masas calculada
	
	//Ask data throught the terminal
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"\tIngrese la masa 1 : ";
	cin>>imputMasa1;
	cout<<"\tIngrese la masa 2 : ";
	cin>>imputMasa2;
	cout<<"\tIngrese la distancia : ";
	cin>>imputDistancia;
	
	//calculation
	imputFuerza=(G*imputMasa1*imputMasa2)/pow(imputDistancia,2.0);
	
	//Show result in terminal
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"\tla fuerza de atraccion entre las dos masas es : "<<imputFuerza;
	
	return 0;
}











