/**
 * @file Ejercicio F 01.cpp
 * @author Leonella Vega (leonella.vega32@gmail.com)
 * @brief Excercise funciones 01:
 		Elabore un algoritmo que dados como datos  de
		entrada el radio y la altura de un cilindro calcular, el rea lateral y el volumen del cilindro.
		A = pi^2radio*altura	V =radio^2pi*altura
 * @version 1.0
 * @date 18/12/21
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416		// Constante Pi 

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
    float imputRadio = 0.0;        
	float imputAltura = 0.0;         
	float resulArea = 0.0;         //area lateral del ciclindro calculada 
	float resulVolumen = 0.0;      //volumen del cilindro  calculada
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateLateralAreaCylinder(float radio, float altura);
float CalculateVolumeCylinder(float radio, float altura);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Escribir el radio del cilindro: ";
	cin>>imputRadio;
	cout<<"\n\rEscribir la altura del cilindro: ";
	cin>>imputAltura;
}
//=====================================================================================================

void Calculate(){
	resulArea = CalculateLateralAreaCylinder(imputRadio,imputAltura);              //area latera = 2*pi*radio*altura
	resulVolumen = CalculateVolumeCylinder(imputRadio,imputAltura);      //volume = pi*radio^2*altura   
}
//=====================================================================================================

void ShowResults(){
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"\n\rEl area lateral del cilindro es : "<<resulArea;
	cout<<"\n\rEl volumen del cilindro es : "<<resulVolumen;
}
//=====================================================================================================


float CalculateLateralAreaCylinder(float radio, float altura){
	return 2.0 * PI * radio * altura;	// area lateral = 2 * pi * radio * h
}

//=====================================================================================================
float CalculateVolumeCylinder(float radio, float altura){
	return PI * pow(radio,2.0) * altura;
}

