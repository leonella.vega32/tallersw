/**
 * @file Ejercicio F 02.cpp
 * @author Leonella Vega (leonella.vega32@gmail.com)
 * @brief Excercise funciones 02:
 		Un maestro desea saber que porcentaje de hombres y que porcentaje
		de mujeres hay en un grupo de estudiantes.
 * @version 1.0
 * @date 18/12/21
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;



/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
	int numberAlumnos = 0;           
	int numberWoman = 0;            
	int numberMen = 0;              
	float porcentageWoman = 0.0;      
	float porcentageMen = 0.0;        
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculatePorcentajeWoman(float numberWoman, float numberAlumnos);
float CalculatePorcentajeMen(float numberMen, float numberAlumnos);
int CalculateNumberMen(int numberAlumnos, int numberWoman);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"\n\r========== Insert Data =========\n\r";
	cout<<"Escriba la cantidad de alumnos : ";
	cin>>numberAlumnos;
	cout<<"\n\rEscriba la cantidad de mujeres : ";
	cin>>numberWoman;
}
//=====================================================================================================

void Calculate(){
	numberMen = CalculateNumberMen(numberAlumnos,numberWoman),
	porcentageWoman = CalculatePorcentajeWoman(numberWoman,numberAlumnos);
	porcentageMen = CalculatePorcentajeMen(numberMen,numberAlumnos); 
}
//=====================================================================================================

void ShowResults(){
	cout<<"\n\r=========== Show Result ==========\n\r";
	cout<<"\n\rLa cantidad de varones es : "<<numberMen;
	cout<<"\n\rPorcentaje de mujeres : "<<porcentageWoman<<"%";
	cout<<"\n\rPorcentaje de varones : "<<porcentageMen<<"%";
}
//=====================================================================================================

int CalculateNumberMen(int numberAlumnos, int numberWoman){
	return numberAlumnos-numberWoman;
}

float CalculatePorcentajeWoman(float numberWoman, float numberAlumnos){
	return ((float)numberWoman)/((float)numberAlumnos)*100;
}

//=====================================================================================================
float CalculatePorcentajeMen(float numberMen, float numberAlumnos){
	return ((float)numberMen/(float)numberAlumnos)*100;
}

